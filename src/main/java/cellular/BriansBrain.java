package cellular;

import cellular.CellAutomaton;
import cellular.CellState;
import datastructure.CellGrid;
import datastructure.IGrid;

import java.util.Random;

public class BriansBrain implements CellAutomaton {
    IGrid currentGeneration;

    public BriansBrain(int rows, int columns){
        currentGeneration= new CellGrid(rows,columns,CellState.DEAD);
        initializeCells();
    }

    @Override
    public CellState getCellState(int row, int column) {
        return currentGeneration.get(row, column);
    }

    @Override
    public void initializeCells() {
        Random random = new Random();
        for (int row = 0; row < this.currentGeneration.numRows(); row++) {
            for (int col = 0; col < this.currentGeneration.numColumns(); col++) {
                if (random.nextBoolean()) {
                    currentGeneration.set(row, col, CellState.ALIVE);
                } else {
                    currentGeneration.set(row, col, CellState.DEAD);
                }
            }
        }
    }


    @Override
    public void step() {
        IGrid nextGeneration = currentGeneration.copy();
        for (int row = 0; row < numberOfRows(); row++) {
            for (int col = 0; col < numberOfColumns(); col++) {
                nextGeneration.set(row, col, getNextCell(row, col));
            }

        }
        currentGeneration = nextGeneration;
    }

    @Override
    public CellState getNextCell(int row, int col) {
        CellState current = getCellState(row, col);
        if (current.equals(CellState.ALIVE)){
            return CellState.DYING;
        }
        if (current.equals(CellState.DYING)) {
            return CellState.DEAD;
        }
        if ((current.equals(CellState.DEAD))&&(countNeighbors(row,col,CellState.ALIVE) == 2)) {
            return CellState.ALIVE;
        }
        return CellState.DEAD;
    }



    @Override
    public int numberOfRows() {
        return currentGeneration.numRows();
    }

    @Override
    public int numberOfColumns() {
        return currentGeneration.numColumns();
    }

    @Override
    public IGrid getGrid() {
        return currentGeneration;
    }


    public int countNeighbors(int row, int col, CellState state) {
        int counter = 0;
        for (int i = -1; i < 2; i++) {
            for (int j = -1; j < 2; j++) {
                if ((i == 0) && (j == 0)) {
                    continue;
                }
                if ((row + i < 0) || (col + j < 0) || (row + i >= numberOfRows()) || (col + j >= numberOfColumns())) {
                    //Checks if neighbour is on the grid
                    continue;
                }
                else if (getCellState(row+i,col+j).equals(CellState.ALIVE)) {
                    counter++;
                }
            }
        }
        return counter;
    }
}